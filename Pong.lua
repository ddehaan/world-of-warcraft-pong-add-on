SLASH_PONG1 = "/pong"
SlashCmdList["PONG"] = function(message, editbox) 
	if message == 'start' then 
		StartPong() 
	end 
	
	if message == 'stop' then 
		StopPong() 
	end 
end

local background = CreateFrame("Frame","background",UIParent)
local ball = CreateFrame("Frame","ball",background)
local paddle1 = CreateFrame("Frame","paddle1",background)
local BallX = 7
local BallY = 7
local BallXDirection = 1
local BallYDirection = 1
local BallXSpeed = 5
local BallYSpeed = 5
local BallSpin = 0
local TotalY
local TotalX
local PaddleY = 0
local PaddleMoveDirection = ''
local PaddleBaseMoveSpeed = 5;
local PaddleMoveSpeed = PaddleBaseMoveSpeed
local Score = 0
local Initialized = 0
local PaddleHeight
local Scoreboard = {}
local ScoreboardCol = 1
local ScoreboardRow = 1

while ScoreboardRow <= 9 do
	while ScoreboardCol <= 5 do
		Scoreboard[ScoreboardCol] = {}
		Scoreboard[ScoreboardCol][ScoreboardRow] = CreateFrame("Frame");
		Scoreboard[ScoreboardCol][ScoreboardRow]:SetWidth(5)
		Scoreboard[ScoreboardCol][ScoreboardRow]:SetHeight(5)
		Scoreboard[ScoreboardCol][ScoreboardRow]:SetPoint("CENTER", UIParent, "CENTER", ScoreboardRow * 5, ScoreboardCol * 5);
		Scoreboard[ScoreboardCol][ScoreboardRow]:SetHeight(5)
		Scoreboard[ScoreboardCol][ScoreboardRow].texture = 	Scoreboard[ScoreboardCol][ScoreboardRow]:CreateTexture()
		Scoreboard[ScoreboardCol][ScoreboardRow].texture:SetAllPoints(Scoreboard[ScoreboardCol][ScoreboardRow])
		Scoreboard[ScoreboardCol][ScoreboardRow].texture:SetTexture(1,1,1)
		Scoreboard[ScoreboardCol][ScoreboardRow]:Hide()
		ScoreboardCol = ScoreboardCol + 1;
	end
	ScoreboardCol = 1
	ScoreboardRow = ScoreboardRow + 1;
end

function InitializePong()
	TotalY = GetScreenHeight()
	TotalX = GetScreenWidth() 
	PaddleHeight = TotalY / 7
	paddle1:SetWidth(10)
	paddle1:SetHeight(PaddleHeight)
	paddle1:SetPoint("RIGHT",0,0)
	paddle1:SetBackdropColor(0,0,0,1);
	paddle1.texture = paddle1:CreateTexture()
	paddle1.texture:SetAllPoints(paddle1)
	paddle1.texture:SetTexture(1,1,1)
	ball:SetWidth(30)
	ball:SetHeight(30)
	ball:SetPoint("LEFT",0,0)
	ball.texture = ball:CreateTexture()
	ball.texture:SetAllPoints(ball)
	ball.texture:SetTexture(1,1,1)
	background:SetWidth(TotalX)
	background:SetHeight(TotalY)
	background:SetPoint("CENTER", UIParent, "CENTER", 0, 0);
	background.texture = background:CreateTexture()
	background.texture:SetAllPoints(background)
	background.texture:SetTexture(0,0,0,.5)

end

function StartPong()
	if Initialized == 0 then
		InitializePong()
		Initialized = 1
	end
	MainMenuBar:Hide()
	paddle1:Show()
	background:Show()
	ball:Show()
	ball:EnableKeyboard()
	ball:SetScript("OnKeyDown", function(self, key) StartPaddleMove(key) end)
	ball:SetScript("OnKeyUp", function(self, key) EndPaddleMove(key) end)
	ball:SetScript("OnUpdate", function() MoveBall(); MovePaddle() end)
end

function StopPong()
	BallXSpeed = 5
	BallYSpeed = 5
	BallX = 0
	BallY = 0
	BallSpin = 0
	BallXDirection = 1
	BallYDirection = 1
	Score = 0
	PaddleY = 0
	PaddleMoveDirection = ''
	ball:Hide()
	paddle1:Hide()
	background:Hide()
	MainMenuBar:Show()
	ball:SetScript("OnUpdate", nil)
	ball:SetScript("OnKeyDown", nil)
	ball:SetScript("OnKeyUp", nil)
end

function MovePaddle()
	PaddleMoveSpeed = PaddleMoveSpeed + .2;
	
	if PaddleMoveDirection == "UP" then
		if PaddleY < TotalY / 2 - PaddleHeight / 2 then
			PaddleY = PaddleY + PaddleMoveSpeed
		end
	end
	
	if PaddleMoveDirection == "DOWN" then
		if PaddleY > TotalY / -2 + PaddleHeight / 2 then
			PaddleY = PaddleY - PaddleMoveSpeed
		end
	end
	
	paddle1:SetPoint("RIGHT", 0, PaddleY)
end

function StartPaddleMove(key) 
	PaddleMoveSpeed = 5;
	PaddleMoveDirection = key
end

function EndPaddleMove(key)
	if (key == PaddleMoveDirection) then
		PaddleMoveDirection = ''
	end
end

function MoveBall()
	if BallXDirection == 1 then
		BallX = BallX + BallXSpeed
	else	
		BallX = BallX - BallXSpeed
	end
	
	if BallYDirection == 1 then
		BallY = BallY + BallYSpeed
	else
		BallY = BallY - BallYSpeed
	end
	
	if BallX >= TotalX - 30 then
		BallXDirection = 0
		CheckPaddlePosition()
		CalculateSpin()
	end

	if BallX <= 0 then
		PlaySoundFile("Interface\\AddOns\\Pong\\sounds\\plop.ogg")
		BallXDirection = 1
		CalculateSpin()
	end
	
	if BallY <= -1 * TotalY / 2 then
		PlaySoundFile("Interface\\AddOns\\Pong\\sounds\\plop.ogg")
		BallYDirection = 1
		CalculateSpin()
	end
	
	if BallY >= TotalY / 2 - 30 then
		PlaySoundFile("Interface\\AddOns\\Pong\\sounds\\plop.ogg")
		BallYDirection = 0
		CalculateSpin()
	end

	ball:SetPoint("LEFT",BallX,BallY)
end

function CheckPaddlePosition()
	if PaddleY + 75 < BallY or PaddleY - 75 > BallY then
		PongLose()
	else
		PlaySoundFile("Interface\\AddOns\\Pong\\sounds\\beep.ogg")
		Score = Score + 1
		BallXSpeed = BallXSpeed + .5;
		BallYSpeed = BallYSpeed + .5;
		if (PaddleMoveDirection == 'UP') then
			BallSpin = PaddleMoveSpeed * .1;
		elseif (PaddleMoveDirection == 'DOWN') then
			BallSpin = PaddleMoveSpeed * -.1;
		else
			BallSpin = 0;
		end
	end
end

function CalculateSpin() 
	BallXSpeed = BallXSpeed + BallSpin
	BallYSpeed = BallYSpeed - BallSpin
	if BallXSpeed < 2 then
		BallXSpeed = 2
	end
	if BallYSpeed < 2 then
		BallYSpeed = 2
	end
	
	print("x: " .. BallXSpeed .. " / y: " .. BallYSpeed .. " / spin: " .. BallSpin);
	BallSpin = BallSpin / 2
end

function PongLose()
	PlaySoundFile("Interface\\AddOns\\Pong\\sounds\\peep.ogg")
	print("You lost!")
	print("Score: " .. Score);
	StopPong()
end
